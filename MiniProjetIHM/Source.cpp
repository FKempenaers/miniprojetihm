﻿#include <opencv2/opencv.hpp>
#include <iostream>
#include <ostream>
#include <fstream>
#include <conio.h>
#include <chrono> 
#include <time.h>
#include <windows.h>


#define WIDTH 640
#define HEIGHT 480

#define NAME_WINDOW "let's catch balloons"

#define CVUI_IMPLEMENTATION


#include "cvui.h"

using namespace std;
using namespace cv;

std::string intToString(int number);
void deplacement(Vec3i &c, Vec2i &v, Mat ballon);
void Menu(String windowname, cv::Mat WindowFrame);
void saveBestScore();
int readBestScore();

cv::Mat fond = cv::imread("fond.jpg");
int low_threshold = 50, high_threshold = 150;
bool use_canny = false;
cv::Mat frame;

int score = 0;

///////////////////////////////////////////////////////////////////////////////////////////////////
int main() {

	cvStartWindowThread();

	// Implementation d'un goto Gamestopped pour recommencer le jeu
	GameStopped:

	VideoCapture cap;
	cv::Mat img;           // output image
	frame = fond.clone();

	cv::Mat imgHSV;
	cv::Mat imgThreshLow;
	cv::Mat imgThreshHigh;
	cv::Mat imgThresh;


	cap.set(CV_CAP_PROP_FRAME_WIDTH, WIDTH);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, HEIGHT);
	cap.open(0);

	namedWindow(NAME_WINDOW, CV_WINDOW_NORMAL);
	setWindowProperty(NAME_WINDOW, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	moveWindow(NAME_WINDOW, 0, 0);
	cvui::init(NAME_WINDOW);


	// Variables "génération de formes"
	Mat ballon = imread("ballon.jpg");
	Mat ballonEx = imread("ballonExp.jpg");
	int nbrBalls = 10, cpt = 0;
	srand(time(0));



	/*############ START MENU #######################################*/
	 Menu(NAME_WINDOW, frame);



	vector<Vec3i> cibles;
	vector<Vec2i> ciblesDir;

	// Initialisation des cibles
	for (int i = 0; i < 10; i++) {
		Vec3i p(rand() % (WIDTH - ballon.cols * 2) + ballon.cols, rand() % (HEIGHT - ballon.rows * 2) + ballon.rows, 40);
		Vec2i v(0, 0);
		cibles.push_back(p);
		ciblesDir.push_back(v);
	}

	
	bool stopped = false;


	long clk_tck = CLOCKS_PER_SEC;
	clock_t t1, t2;

	t1 = clock();
	long temps;

	do {
		cap >> img; // Webcam >> Matrice
		flip(img, img, 1);
		/*
				cv::Mat edges; // Matrice de travail
				cvtColor(img, edges, CV_BGR2GRAY); // Matrice couleur > Matrice GS
				GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5); // Floute l'image
				*/

		cv::cvtColor(img, imgHSV, CV_BGR2HSV);
		cv::inRange(imgHSV, cv::Scalar(0, 155, 155), cv::Scalar(18, 255, 255), imgThreshLow);
		cv::inRange(imgHSV, cv::Scalar(165, 155, 155), cv::Scalar(179, 255, 255), imgThreshHigh);

		cv::add(imgThreshLow, imgThreshHigh, imgThresh);

		cv::GaussianBlur(imgThresh, imgThresh, cv::Size(3, 3), 0);

		cv::Mat structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));

		cv::dilate(imgThresh, imgThresh, structuringElement);
		cv::erode(imgThresh, imgThresh, structuringElement);



		Point center(-100, -100);


		stopped = cvui::button(img, 300, 0, "STOP GAME");

		// Deplacement aléatoire de chaque cible
		for (size_t i = 0; i < 10; i++) {
			deplacement(cibles[i], ciblesDir[i], ballon);
			ballon.copyTo(img(cv::Rect(cibles[i][0], cibles[i][1], ballon.cols, ballon.rows)));
			//circle(img, Point(cibles[i][0], cibles[i][1]), cibles[i][2], Scalar(255, 0, 0), 3, 8, 0);
		}

		vector<Vec3f> circles; // Tableau pour stocker les cercles d�tect�s
		HoughCircles(imgThresh,			// input image
			circles,						// function output (must be a standard template library vector
			CV_HOUGH_GRADIENT,				// two-pass algorithm for detecting circles, this is the only choice available
			2,								// size of image / this value = "accumulator resolution", i.e. accum res = size of image / 2
			imgThresh.rows / 4,				// min distance in pixels between the centers of the detected circles
			100,							// high threshold of Canny edge detector (called by cvHoughCircles)						
			50,								// low threshold of Canny edge detector (set at 1/2 previous value)
			10,								// min circle radius (any circles with smaller radius will not be returned)
			400);

		int centreX = -100;
		int centreY = -100;

		// Dessine les cercles
		for (size_t i = 0; i < circles.size(); i++) // Pour chaque cercle d�tect�
		{
			Point center((int)(circles[i][0]), (int)(circles[i][1]));  // Trouve le centre du cercle i
			int radius = cvRound(circles[i][2]); // Calcule le rayon du cercle i
			centreX = circles[i][0];
			centreY = circles[i][1];

			// Dessine le marqueur
			line(img, Point(center.x + 5, center.y + 5), Point(center.x + 12, center.y + 12), Scalar(0, 255, 0), 2, 8, 0);
			line(img, Point(center.x - 5, center.y + 5), Point(center.x - 12, center.y + 12), Scalar(0, 255, 0), 2, 8, 0);
			line(img, Point(center.x + 5, center.y - 5), Point(center.x + 12, center.y - 12), Scalar(0, 255, 0), 2, 8, 0);
			line(img, Point(center.x - 5, center.y - 5), Point(center.x - 12, center.y - 12), Scalar(0, 255, 0), 2, 8, 0);

		}

		// D�tection de cible touch�e
		for (size_t j = 0; j < cibles.size(); j++) // Pour chaque cible non-touch�e
		{
			// Cibles circulaires
			int distance = (sqrt(pow(cibles[j][0] + ballon.cols / 2 - centreX, 2) + pow(cibles[j][1] + ballon.rows / 2 - centreY, 2)));
			if (cibles[j][2] >= distance) // Si le marqueur est � l'int�rieur de la cible j de rayon cible[j][2] 
			{
				// Dessin d'un ballon touche
				ballonEx.copyTo(img(cv::Rect(cibles[j][0], cibles[j][1], ballonEx.cols, ballonEx.rows)));


				// On affiche une nouvelle cible
				cibles[j][0] = rand() % (WIDTH - ballon.cols * 2) + ballon.cols;
				cibles[j][1] = rand() % (HEIGHT - ballon.rows * 2) + ballon.rows;
				cibles[j][2] = 40;
				score++; // On incremente le score
			}
		}

		t2 = clock();

		temps = (t2 - t1) / (clk_tck);

		cvui::printf(img, 1, 1, 0.5, 0xff0000, "Cibles touchee : %d", score);
		cvui::printf(img, 500, 0, 0.5, 0xff0000, "temps : %d", temps);




		cout << "temps : " << temps << endl;
		cout << "score : " << score << endl;
		imshow(NAME_WINDOW, img); // Affiche la matrice img



		//imshow(NAME_WINDOW, img); // Affiche la matrice img

		waitKey(20);



	} while (temps < 5 && !stopped);

		cv::putText(frame, "your score is :" + intToString(score), Point(120, 200), FONT_HERSHEY_SIMPLEX, 1, Scalar(2, 13, 120), 3, 8);
			
		int bestScore = readBestScore();

		cv::putText(frame, "the best score is : " + intToString(bestScore), Point(120, 300), FONT_HERSHEY_SIMPLEX, 1, Scalar(2, 13, 120), 3, 8);
		cv::putText(frame, "Press esc to access the menu", Point(120, 350), FONT_HERSHEY_SIMPLEX, 1, Scalar(2, 13, 120), 3, 8);
		imshow(NAME_WINDOW, frame);
		waitKey(0);
		goto GameStopped;

	return(0);
}

/*fonction de conversion int en string pour l'affichage du score */

std::string intToString(int number) {
	std::ostringstream oss;
	oss << number;
	return oss.str();
}


/*Fonction de deplacement des personnages
Une cible a des attributs stock�s dans 2 vecteurs, le 1er stocke les coords x et y, le 2nd la direction (0 haut - 1 droite - 2 bas - 3 gauche)
et un nombre de fois o� avancer dans la direction, lorsque ce nombre est � 0 la direction est chang�e et un nouveau nombre est assign�.
*/
void deplacement(Vec3i &p, Vec2i &v, Mat ballon) {

	/*Doit on encore se d�placer dans cette direction ?
	Sinon on change de direction et on choisi combien de fois avancer dans la nouvelle direction*/

	if (v[1] > 0) {
		v[1]--;
	}
	else {
		v[0] = rand() % 4;
		v[1] = rand() % 10 + 10;
	}

	/*Se d�placer selon la direction contenue dans v[0] */
	switch (v[0]) {
	case 0:
		//Haut
		if (p[1] < HEIGHT - ballon.rows * 2)
			p[1] += ballon.rows;
		else
			v[1] = 0;
		break;
	case 1:
		//Droite
		if (p[0] < WIDTH - ballon.cols * 2)
			p[0] += ballon.cols;
		else
			v[1] = 0;
		break;
	case 2:
		//Bas
		if (p[1] > ballon.rows * 2)
			p[1] -= ballon.rows;
		else
			v[1] = 0;
		break;
	case 3:
		//Gauche
		if (p[0] > ballon.cols * 2)
			p[0] -= ballon.cols;
		else
			v[1] = 0;
		break;
	}

	/*
	Point(x,y,dir(0-3),nbframes);


	*/


}

void Menu(String windowname, cv::Mat frame) {

	cv::Mat instructions;

	while (!cvui::button(frame, 500, 180, "START GAME")) {
		// Should we apply Canny edge?



		cvui::button(frame, 500, 180, "START GAME");

		if (cvui::button(frame, 500, 260, "INSTRUCTIONS")) {
			const char text_filename[] = "Instructions.pdf";
			const char text_application[] = "start firefox";

			std::string system_str;
			system_str = text_application;
			system_str += " ";
			system_str += text_filename;

			// Execute the application
			system(system_str.c_str());
		}

		if (cvui::button(frame, 500, 220, "QUIT GAME")) {
			saveBestScore();
			exit(EXIT_SUCCESS);
		}


		// Mise à jour du frame
		cvui::update();

		// Show everything on the screen
		cv::imshow(NAME_WINDOW, frame);

		// Check if ESC key was pressed
		if (cv::waitKey(20) == 27) {
			break;
		}
	}


}

int readBestScore() {

	string savedScore;
	ifstream infile;
	infile.open("Scores.txt");

	cout << "Reading from the file" << endl;
	infile >> savedScore;

	int saved = stoi(savedScore);


	return saved;
}
void saveBestScore() {

	
	int saved = readBestScore();

	if (score > saved) {

		ofstream myfile;
		myfile.open("Scores.txt");
		if (myfile.is_open())
			myfile << score;
		else
			cout << "can't open file";

		myfile.close();
	}



}