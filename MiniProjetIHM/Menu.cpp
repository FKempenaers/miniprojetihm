#define CVUI_IMPLEMENTATION
#include <opencv2/opencv.hpp>
#include "cvui.h"

#define WINDOW_NAME "GAME MENU"

using namespace std;
using namespace cv;

int main(void)
{
	Mat Output;
	cv::Mat frame = cv::Mat(200, 500, CV_8UC3);
	cv::VideoCapture camera(0);
		
	if (!camera.isOpened())
		return 1;

	int count = 0;

	// Init a OpenCV window and tell cvui to use it.
	cv::namedWindow(WINDOW_NAME);
	cvui::init(WINDOW_NAME);

	while (true) {
		// Fill the frame with a nice color
		Mat frame2;
		camera >> frame2;
		cvtColor(frame2, Output, CV_BGR2GRAY);
		GaussianBlur(Output, Output, Size(7, 7), 1.5, 1.5);
		Canny(Output, Output, 0, 30, 3);
	

		// Show a button at position (110, 80)
		if (cvui::button(Output, 0, 0, "START GAME")) {
			// The button was clicked, so let's increment our counter.
			count++;
		}
		
		if (cvui::button(Output, 150, 0, "START GAME")) {
			// The button was clicked, so let's increment our counter.
			count++;
		}
		
		if (cvui::button(Output, 300, 0, "STOP GAME")) {
			// The button was clicked, so let's increment our counter.
			count++;
		}

		// Show how many times the button has been clicked.
		// Text at position (250, 90), sized 0.4, in red.
		cvui::printf(Output, 250, 90, 0.4, 0xff0000, "Button click count: %d", count);

		// Update cvui internal stuff
		cvui::update();

		// Show everything on the screen
		cv::imshow(WINDOW_NAME, Output);

		// Check if ESC key was pressed
		if (cv::waitKey(20) == 27) {
			break;
		}
	}
	return 0;
}